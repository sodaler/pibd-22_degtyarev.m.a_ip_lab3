package ru.ulstu.is.sbapp.worker.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.model.Worker;
import ru.ulstu.is.sbapp.worker.service.WorkerService;

import java.util.List;

@RestController
@RequestMapping("/worker")
public class WorkerController {
    private final WorkerService workerService;

    public WorkerController(WorkerService workerService) {
        this.workerService = workerService;
    }

    @GetMapping("/{id}")
    public Worker getWorker(@PathVariable Long id) {
        return workerService.findWorker(id);
    }

    @GetMapping("/")
    public List<Worker> getWorkers() {
        return workerService.findAllWorkers();
    }

    @PostMapping("/")
    public Worker createWorker(@RequestParam("firstName") String firstName,
                               @RequestParam("lastName") String lastName,
                               @RequestParam("serial") int serial,
                               @RequestParam("number") int number,
                               @RequestParam("city") String city) {
        return workerService.addWorker(firstName, lastName, serial, number, city);
    }

    @PatchMapping("/{id}")
    public Worker updateWorker(@PathVariable Long id,
                               @RequestParam("firstName") String firstName,
                               @RequestParam("lastName") String lastName,
                               @RequestParam("serial") int serial,
                               @RequestParam("number") int number,
                               @RequestParam("city") String city){
        return workerService.updateWorker(id, firstName, lastName, serial, number, new City(city));
    }

    @DeleteMapping("/{id}")
    public Worker deleteWorker(@PathVariable Long id) {
        return workerService.deleteWorker(id);
    }
}