package ru.ulstu.is.sbapp.worker.controller;

import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.service.CityService;

import java.util.List;

@RestController
@RequestMapping("/city")
public class CityController {
    private final CityService cityService;

    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping("/{id}")
    public City getCity(@PathVariable Long id) {
        return cityService.findCity(id);
    }

    @GetMapping("/")
    public List<City> getCities() {
        return cityService.findAllCities();
    }

    @PostMapping("/")
    public City createCity(@RequestParam("firstName") String firstName,
                           @RequestParam("lastName") String name
                                 ) {
        return cityService.addCity(name);
    }

    @PatchMapping("/{id}")
    public City updateCity(@PathVariable Long id,
                           @RequestParam("firstName") String name
                             ) {
        return cityService.updateCity(id, name);
    }

    @DeleteMapping("/{id}")
    public City deleteCity(@PathVariable Long id) {
        return cityService.deleteCity(id);
    }
}