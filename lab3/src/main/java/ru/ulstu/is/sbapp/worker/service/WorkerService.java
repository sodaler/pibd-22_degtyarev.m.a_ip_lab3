package ru.ulstu.is.sbapp.worker.service;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.model.Worker;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class WorkerService {
    @PersistenceContext
    private EntityManager em;

    private final CityService cityService;

    public WorkerService(CityService cityService) {
        this.cityService = cityService;
    }

    @Transactional
    public Worker addWorker(String firstName, String lastName, int serial, int number, String ct) {
        if (!StringUtils.hasText(firstName) || !StringUtils.hasText(lastName) || !StringUtils.hasText(String.valueOf(serial)) || !StringUtils.hasText(String.valueOf(number))
        || !StringUtils.hasText(ct)) {
            throw new IllegalArgumentException("Worker data is null or empty");
        }
        City city = cityService.findCityByName(ct);
        Worker worker = new Worker(firstName, lastName, serial, number, city);
        em.persist(worker);
        return worker;
    }

    @Transactional(readOnly = true)
    public Worker findWorker(Long id) {
        final Worker worker = em.find(Worker.class, id);
        if (worker == null) {
            throw new EntityNotFoundException(String.format("Worker with id [%s] is not found", id));
        }
        return worker;
    }

    @Transactional(readOnly = true)
    public List<Worker> findAllWorkers() {
        return em.createQuery("select s from Worker s", Worker.class)
                .getResultList();
    }

    @Transactional
    public Worker updateWorker(Long id, String firstName, String lastName, int serial, int number, City ct) {
        if (!StringUtils.hasText(firstName) || !StringUtils.hasText(lastName) || !StringUtils.hasText(String.valueOf(serial))
                || !StringUtils.hasText(String.valueOf(number)) || !StringUtils.hasText(ct.getName())) {
            throw new IllegalArgumentException("Worker name is null or empty");
        }
        final Worker currentWorker = findWorker(id);
        City city = cityService.findCityByName(ct.getName());

        currentWorker.setFirstName(firstName);
        currentWorker.setLastName(lastName);
        currentWorker.setPassport(serial, number);
        currentWorker.setCity(city);

        return em.merge(currentWorker);
    }

    @Transactional
    public Worker deleteWorker(Long id) {
        final Worker currentWorker = findWorker(id);
        em.remove(currentWorker);
        return currentWorker;
    }

    @Transactional
    public void deleteAllWorkers() {
        em.createQuery("delete from Worker").executeUpdate();
    }
}
