package ru.ulstu.is.sbapp.worker.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.worker.model.City;

import javax.persistence.*;
import java.util.List;

@Service
public class CityService {
    private final Logger log = LoggerFactory.getLogger(CityService.class);

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public City addCity(String name) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("City data is null or empty");
        }
        final City city = new City(name);
        em.persist(city);
        return city;
    }

    @Transactional(readOnly = true)
    public City findCity(Long id) {
        final City city = em.find(City.class, id);
        if (city == null) {
            throw new EntityNotFoundException(String.format("City with id [%s] is not found", id));
        }
        return city;
    }

    //this
    @Transactional(readOnly = true)
    public City findCityByName(String name) {
        try {
            return em.createQuery("select s from City s where s.name = :name", City.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException | NonUniqueResultException e) {
            throw new EntityNotFoundException(String.format("City with name [%s] is not found", name));
        }
    }

    @Transactional(readOnly = true)
    public List<City> findAllCities() {
        return em.createQuery("select s from City s", City.class)
                .getResultList();
    }

    @Transactional
    public City updateCity(Long id, String name) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("City name is null or empty");
        }
        final City currentCity = findCity(id);
        currentCity.setName(name);
        return em.merge(currentCity);
    }

    @Transactional
    public City deleteCity(Long id) {
        City currentCity = findCity(id);
        em.remove(currentCity);
        return currentCity;
    }

    @Transactional
    public void deleteAllCitiesUnsafe() {
        log.warn("Unsafe usage!");
        List<City> cities = findAllCities();
        for(City city : cities){
            if(city.getWorkers().size()>0)
                city.removeAllWorkers();
        }
        em.createQuery("delete from City").executeUpdate();
    }

    @Transactional
    public void deleteAllCities() throws InCityFoundWorkersException {
        List<City> cities = findAllCities();
        for(City city : cities){
            if(city.getWorkers().size()>0)
                throw new InCityFoundWorkersException("в городе " + city.getName() + "имеются рабочие");
        }
        em.createQuery("delete from City").executeUpdate();
    }
}
