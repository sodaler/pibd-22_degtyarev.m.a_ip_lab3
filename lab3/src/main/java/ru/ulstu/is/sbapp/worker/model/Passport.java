package ru.ulstu.is.sbapp.worker.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Passport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int serial;
    private int number;

    public Passport(){ }

    public Passport(int serial, int number){
        this.number = number;
        this.serial = serial;
    }

    public int getSerial() {
        return serial;
    }

    public int getNumber() {
        return number;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passport passport = (Passport) o;
        return Objects.equals(id, passport.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Passport{" +
                "id=" + id +
                ", serial='" + serial + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}

